#include <iostream>
#include <fstream>
#include <cstdio>
#include <string>
#include <ctime>

using namespace std;

const char alphabet[] = "0123456789abcdefghijklmnopqrstuvwxyz";
const int MIN_SIZE_PASS = 2;
const int MAX_SIZE_PASS = 6;

bool is_valid_password(string _pass);
string brootforce(char *_password, unsigned &_countIterations);
string brootforce(char *_password, unsigned _maxIterations, unsigned &_countIterations);

int main(int argc, char **argv) {
	string pass, brootPassword;

	bool isValidPassord = false;
	while (!isValidPassord) {
		cout << "Enter your password (2 - 6 symbol): ";
		getline(cin, pass);
		isValidPassord = is_valid_password(pass);
	}

	char *password = new char[pass.size()];
	for (unsigned count = 0; count < pass.size(); ++count)
		password[count] = pass[count];
	password[pass.size()] = '\0';
	
	char answer;
	unsigned maxItteration = -1, countIterations;
	cout << "Are you want to limit the number of iterations? (y/n): ";
	cin >> answer;
	if (answer == 'y' || answer == 'Y') {
		cout << "Enter max quantity of itterations: ";
		cin >> maxItteration;
	}
	
	ofstream fout;
	fout.open("result.txt", ios_base::trunc | ios_base::out);
	if (!fout.is_open()) {
		cout << "File is not open!\n";
		return 1;
	}

	cout << "Processing...\n";

	int startTime, endTime;
	try {
		if (maxItteration == -1) {
			startTime = clock();
			brootPassword = brootforce(password, countIterations);
			endTime = clock();
		}
		else {
			startTime = clock();
			brootPassword = brootforce(password, maxItteration, countIterations);
			endTime = clock();
		}
		if (countIterations == maxItteration)
			throw "Maximum number of iterations exceeded! Password is not picked up!";
		fout << "Results of the program.\n\n"
			<< "Number of iterations: " << countIterations << "\n"
			<< "Time: " << (endTime - startTime) << "ms\n"
			<< "Your password: " << password << "\n"
			<< "Broot password: " << brootPassword << endl;
	}
	catch (char *errorStr) {
		fout << "Results of the program.\n\n"
			<< "Number of iterations: " << countIterations << "\n"
			<< "Time: " << (endTime - startTime) << "ms\n"
			<< errorStr << endl;
	}
	
	fout.close();

	cout << "The program finished work.\n";

	system("pause");
	return 0;
}

bool is_valid_password(string _pass) {
	if (_pass.size() < MIN_SIZE_PASS) {
		cout << "Your password is too short!\n";
		return false;
	}
	if (_pass.size() > MAX_SIZE_PASS) {
		cout << "Your password it too long!\n";
		return false;
	}
	for (unsigned count = 0; count < _pass.size(); ++count) {
		if (strchr(alphabet, _pass[count]) == NULL) {
			cout << "Unacceptable symbols\n";
			return false;
		}
	}
	return true;
}

string brootforce(char *_password, unsigned &_countIterations) {
	_countIterations = 1;

	int brootPassSize = MIN_SIZE_PASS;
	while (true) {
		int *arrayPtr = new int[brootPassSize];
		char *brootPass = new char[brootPassSize + 1];
		char *lastValidPass = new char[brootPassSize + 1];

		for (int count = 0; count < brootPassSize; ++count)
			arrayPtr[count] = 0;
		for (int count = 0; count < brootPassSize; ++count)
			brootPass[count] = 0;
		brootPass[brootPassSize] = '\0';
		for (int count = 0; count < brootPassSize; ++count)
			lastValidPass[count] = alphabet[(sizeof(alphabet) / sizeof(char)) - 2];
		lastValidPass[brootPassSize] = '\0';

		bool flag = true;

		while (flag) {
			for (int countAlphabet = 0; countAlphabet < ((sizeof(alphabet) / sizeof(char)) - 1); ++countAlphabet) {
				arrayPtr[0] = countAlphabet;
				for (int count = 0; count < brootPassSize; count++)
					brootPass[count] = alphabet[arrayPtr[count]];

				if ((strcmp(_password, brootPass)) == NULL) {
					string _brootPass;
					for (int count = 0; count < brootPassSize; ++count)
						_brootPass.push_back(brootPass[count]);
					return _brootPass;
				}

				_countIterations++;

				if ((strcmp(brootPass, lastValidPass)) == NULL)
					flag = false;

			}
			for (int countArray = 1; countArray < brootPassSize; ++countArray) {
				arrayPtr[countArray]++;
				if (arrayPtr[countArray] >= ((sizeof(alphabet) / sizeof(char)) - 1))
					arrayPtr[countArray] = 0;
				else
					break;
			}
		}
		delete[] lastValidPass;
		delete[] brootPass;
		delete[] arrayPtr;
		++brootPassSize;
	}
}

string brootforce(char *_password, unsigned _maxIterations, unsigned &_countIterations) {
	_countIterations = 1;

	int brootPassSize = MIN_SIZE_PASS;
	while (true) {
		int *arrayPtr = new int[brootPassSize];
		char *brootPass = new char[brootPassSize + 1];
		char *lastValidPass = new char[brootPassSize + 1];

		for (int count = 0; count < brootPassSize; ++count)
			arrayPtr[count] = 0;
		for (int count = 0; count < brootPassSize; ++count)
			brootPass[count] = 0;
		brootPass[brootPassSize] = '\0';
		for (int count = 0; count < brootPassSize; ++count)
			lastValidPass[count] = alphabet[(sizeof(alphabet) / sizeof(char)) - 2];
		lastValidPass[brootPassSize] = '\0';
		
		bool flag = true;
		
		while (flag) {
			for (int countAlphabet = 0; countAlphabet < ((sizeof(alphabet) / sizeof(char)) - 1); ++countAlphabet) {
				arrayPtr[0] = countAlphabet;
				for (int count = 0; count < brootPassSize; count++)
					brootPass[count] = alphabet[arrayPtr[count]];

				if ((strcmp(_password, brootPass)) == NULL) {
					string _brootPass;
					for (int count = 0; count < brootPassSize; ++count)
						_brootPass.push_back(brootPass[count]);
					return _brootPass;
				}

				if (_countIterations == _maxIterations)
					return string();
				_countIterations++;

				if ((strcmp(brootPass, lastValidPass)) == NULL)
					flag = false;
					
			}
			for (int countArray = 1; countArray < brootPassSize; ++countArray) {
				arrayPtr[countArray]++;
				if (arrayPtr[countArray] >= ((sizeof(alphabet) / sizeof(char)) - 1))
					arrayPtr[countArray] = 0;
				else
					break;
			}
		}
		delete[] lastValidPass;
		delete[] brootPass;
		delete[] arrayPtr;
		++brootPassSize;
	}
}